const form = document.querySelector('.password-form');

form.addEventListener('click', function(event) {
    if(event.target.tagName === 'I') {
        event.target.classList.toggle('fa-eye-slash');

        const inputField =  event.target.previousElementSibling;
        inputField.setAttribute(
            'type',
            inputField.getAttribute('type') ===
            'password' ? 'text' : 'password'
        );  
    }
});

form.addEventListener('submit', function(event) {
    event.preventDefault();
    const inputFirst = document.getElementById('js-first-password');
    const inputSecond = document.getElementById('js-second-password');
    const hidden = document.querySelector('.error');
    if(inputFirst.value === inputSecond.value) {
       alert ("You are welcome!")
       hidden.classList.add('hidden');
    } else {
        hidden.classList.remove('hidden');
    }
});





